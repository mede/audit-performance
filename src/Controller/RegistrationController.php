<?php

namespace App\Controller;

use App\Form\{UserType,EmailResetType,ResetPasswordType};
use App\Entity\User;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\{RedirectResponse, Request, Response};
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegistrationController extends AbstractController
{
    /**
     * Action : handle user registration
     * @Route("/register", name="user_registration")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setIsActive(true);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Votre compte à bien été enregistré.');
            return $this->redirectToRoute('login');
        }
        return $this->render('registration/register.html.twig', ['form' => $form->createView(), 'mainNavRegistration' => true, 'title' => 'Inscription']);
    }

    /**
     * Action : handle password recovery
     * @Route("/forgotten_pass", name="forgotten_password")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @return RedirectResponse|Response
     */
    public function forgottenPassword(Request $request, Swift_Mailer $mailer)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(EmailResetType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $user User
             */
            $user = $entityManager->getRepository(User::class)->findOneByEmail($form->getData()['email']);

            if ($user === null) {
                $this->addFlash('danger', 'Email Inconnu');
                return $this->redirectToRoute('trick_index');
            }
            $token = uniqid();
            try {
                $user->setResetToken($token);
                $entityManager->persist($user);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
                return $this->redirectToRoute('trick_index');
            }

            $url = $this->generateUrl('reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);

            $message = (new Swift_Message('Mot de passe oublié'))
                ->setFrom('thabti.moez@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    "Cliquez sur ce lien pour redéfinir un nouveau mot de passe : " . $url,
                    'text/html'
                );

            $mailer->send($message);

            $this->addFlash('notice', 'Mail envoyé');

            return $this->redirectToRoute('trick_index');
        }
        return $this->render('security/forgotten_password.html.twig', [
            'form' => $form->createView(),
            'title' => 'Recupérer mot de passe'
        ]);
    }

    /**
     * Action : handle setting up new password
     * @Route("/reset_pass", name="reset_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return RedirectResponse|Response
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $token = $request->query->get('token');
        if ($token !== null) {
            $entityManager = $this->getDoctrine()->getManager();

            /**
             * @var $user User
             */
            $user = $entityManager->getRepository(User::class)->findOneByToken($token);
            if ($user !== null) {
                $form = $this->createForm(ResetPasswordType::class, $user);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
                    $user->setPassword($encoded);
                    $entityManager->persist($user);
                    $entityManager->flush();

                    $this->addFlash('notice', 'Mot de passe mis à jour');
                    return $this->redirectToRoute('login');
                }

                return $this->render('security/reset_password.html.twig', array('form' => $form->createView()));
            }
        }

        $this->addFlash('danger', 'Token Inconnu');
        return $this->redirectToRoute('trick_index');
    }
}
