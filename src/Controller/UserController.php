<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\{RedirectResponse,Response,Request};
use Symfony\Component\Routing\Annotation\Route;
use App\Service\{TemporaryFileManager, UploadedFileManager};

class UserController extends AbstractController
{
    /**
     * @Route("/member/profile-details", name="profil_show")
     */
    public function show()
    {
        return $this->render('user/profile.html.twig');
    }

    /**
     * @Route("/member/profile/{id}", name="profil_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @param TemporaryFileManager $temporaryStorage
     * @param UploadedFileManager $uploadedFile
     * @return RedirectResponse|Response
     */
    public function edit(Request $request, User $user, TemporaryFileManager $temporaryStorage, UploadedFileManager $uploadedFile)
    {
        if (null !== $user->getPicture()) {
            $temporaryStorage->setTempImg($user->getPicture());
            $user->setPicture($temporaryStorage->getTempImg());
        }

        $form = $this->createForm(ProfileType::class, $user);

        if ($request->isMethod('post')) {
            $storedImage = $temporaryStorage->getTempImg();
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('photo')->getData() == null && isset($storedImage)) {
                $file2Save = $uploadedFile->docsInputManager($storedImage);
                $user->setPicture($file2Save);
            } else {
                $file2Save = $uploadedFile->docsInputManager([$form->get('photo')->getData()]);
                $user->setPicture($file2Save);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Votre profil a été mis à jour.');
            return $this->redirectToRoute('profil_show');
        }

        return $this->render('user/profile_edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
