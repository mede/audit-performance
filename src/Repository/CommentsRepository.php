<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry$registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * @param $trickId
     * @param int $firstResult
     * @return mixed
     */
    public function findComments($trickId, $firstResult=0)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.trick = :id')
            ->setParameter('id', $trickId)
            ->setMaxResults(5)
            ->setFirstResult($firstResult)
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}
