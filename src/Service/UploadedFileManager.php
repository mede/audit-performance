<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

class UploadedFileManager
{
    private $rootDirectory;

    public function __construct($rootDirectory)
    {
        $this->rootDirectory = $rootDirectory;
    }

    /**
     * @return string
     */
    public function generateUniqueFileName()
    {
        return md5(uniqid());
    }

    /**
     * generate unique names for docs and manage their storage
     * @param $docs
     * @return array
     */
    public function docsInputManager($docs)
    {
        $arrayOfDocs = [];

        if (!empty($docs)) {
            foreach ($docs as $file) {
                $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();
                $arrayOfDocs[] = $fileName;
                $file->move($this->rootDirectory . '/public/uploads/images', $fileName);
            }
        }

        return $arrayOfDocs;
    }
}
