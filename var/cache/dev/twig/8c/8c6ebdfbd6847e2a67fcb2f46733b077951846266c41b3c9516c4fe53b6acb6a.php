<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* trick/_form.html.twig */
class __TwigTemplate_6cab23fe493df5c474ceab5d367b18c1cd17b0e70c30ac5bcd4c9a3908b235fb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
<div class=\"container\">
    <div>";
        // line 3
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 3, $this->source); })()), "name", [], "any", false, false, false, 3), 'row');
        echo "</div>
    <div>";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "description", [], "any", false, false, false, 4), 'row');
        echo "</div
    <div class=\"row\">
        <div class=\"col-md-6\">";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 6, $this->source); })()), "niveau", [], "any", false, false, false, 6), 'row');
        echo "</div>
        <div class=\"col-md-6\">";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), "trick_group", [], "any", false, false, false, 7), 'row');
        echo "</div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-6\">
            <ul id=\"img-fields-list\"
                data-prototype=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), "imgDocs", [], "any", false, false, false, 12), "vars", [], "any", false, false, false, 12), "prototype", [], "any", false, false, false, 12), 'widget'));
        echo "\"
                data-widget-tags=\"";
        // line 13
        echo twig_escape_filter($this->env, "<li></li>");
        echo "\"
                data-widget-counter=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "children", [], "any", false, false, false, 14)), "html", null, true);
        echo "\">
                ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "imgDocs", [], "any", false, false, false, 15));
        foreach ($context['_seq'] as $context["_key"] => $context["doc"]) {
            // line 16
            echo "                    <li>
                        ";
            // line 17
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'widget');
            echo "
                        ";
            // line 18
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'errors');
            echo "
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doc'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "            </ul>
            <button type=\"button\"
                    class=\"add-another-collection-img btn btn-info\"
                    data-list-selector=\"#img-fields-list\">Ajouter une image
            </button>
        </div>
        <div class=\"col-md-6\">
            <ul id=\"doc-fields-list\"
                data-prototype=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), "videoDocs", [], "any", false, false, false, 29), "vars", [], "any", false, false, false, 29), "prototype", [], "any", false, false, false, 29), 'widget'));
        echo "\"
                data-widget-tags=\"";
        // line 30
        echo twig_escape_filter($this->env, "<li></li>");
        echo "\"
                data-widget-counter=\"";
        // line 31
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "children", [], "any", false, false, false, 31)), "html", null, true);
        echo "\">
                ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 32, $this->source); })()), "videoDocs", [], "any", false, false, false, 32));
        foreach ($context['_seq'] as $context["_key"] => $context["doc"]) {
            // line 33
            echo "                    <li>
                        ";
            // line 34
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'widget');
            echo "
                        ";
            // line 35
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'errors');
            echo "
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doc'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "            </ul>
            <button type=\"button\"
                    class=\"add-another-collection-video btn btn-info\"
                    data-list-selector=\"#doc-fields-list\">Ajouter une vidéo
            </button>
        </div>
    </div>
</div>


<button class=\"btn btn-light-green btn-sm\">";
        // line 48
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 48, $this->source); })()), "Valider")) : ("Valider")), "html", null, true);
        echo "</button>
";
        // line 49
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 49, $this->source); })()), 'form_end');
        echo "

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "trick/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 49,  155 => 48,  143 => 38,  134 => 35,  130 => 34,  127 => 33,  123 => 32,  119 => 31,  115 => 30,  111 => 29,  101 => 21,  92 => 18,  88 => 17,  85 => 16,  81 => 15,  77 => 14,  73 => 13,  69 => 12,  61 => 7,  57 => 6,  52 => 4,  48 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}
<div class=\"container\">
    <div>{{ form_row(form.name) }}</div>
    <div>{{ form_row(form.description) }}</div
    <div class=\"row\">
        <div class=\"col-md-6\">{{ form_row(form.niveau) }}</div>
        <div class=\"col-md-6\">{{ form_row(form.trick_group) }}</div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-6\">
            <ul id=\"img-fields-list\"
                data-prototype=\"{{ form_widget(form.imgDocs.vars.prototype)|e }}\"
                data-widget-tags=\"{{ '<li></li>'|e }}\"
                data-widget-counter=\"{{ form.children|length }}\">
                {% for doc in form.imgDocs %}
                    <li>
                        {{ form_widget(doc) }}
                        {{ form_errors(doc) }}
                    </li>
                {% endfor %}
            </ul>
            <button type=\"button\"
                    class=\"add-another-collection-img btn btn-info\"
                    data-list-selector=\"#img-fields-list\">Ajouter une image
            </button>
        </div>
        <div class=\"col-md-6\">
            <ul id=\"doc-fields-list\"
                data-prototype=\"{{ form_widget(form.videoDocs.vars.prototype)|e }}\"
                data-widget-tags=\"{{ '<li></li>'|e }}\"
                data-widget-counter=\"{{ form.children|length }}\">
                {% for doc in form.videoDocs %}
                    <li>
                        {{ form_widget(doc) }}
                        {{ form_errors(doc) }}
                    </li>
                {% endfor %}
            </ul>
            <button type=\"button\"
                    class=\"add-another-collection-video btn btn-info\"
                    data-list-selector=\"#doc-fields-list\">Ajouter une vidéo
            </button>
        </div>
    </div>
</div>


<button class=\"btn btn-light-green btn-sm\">{{ button_label|default('Valider') }}</button>
{{ form_end(form) }}

", "trick/_form.html.twig", "/usr/share/nginx/html/templates/trick/_form.html.twig");
    }
}
