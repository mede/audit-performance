<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* trick/ajax.html.twig */
class __TwigTemplate_5e7a2a5376d439fb1ed35551cc94dd29077a42fa2ff9584baa54b6d79b9a2b71 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/ajax.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/ajax.html.twig"));

        // line 1
        if ( !(((isset($context["tricks"]) || array_key_exists("tricks", $context))) ? (_twig_default_filter((isset($context["tricks"]) || array_key_exists("tricks", $context) ? $context["tricks"] : (function () { throw new RuntimeError('Variable "tricks" does not exist.', 1, $this->source); })()))) : (""))) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 2, $this->source); })()), 0, [], "array", false, false, false, 2), "count", [], "any", false, false, false, 2), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tricks"]) || array_key_exists("tricks", $context) ? $context["tricks"] : (function () { throw new RuntimeError('Variable "tricks" does not exist.', 4, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["trick"]) {
                // line 5
                echo "        <div class=\"col-12 col-md-6 col-lg-3 m-0 p-3\">
            <div class=\"card\">
                ";
                // line 7
                if (twig_test_empty(twig_get_attribute($this->env, $this->source, $context["trick"], "imgDocs", [], "any", false, false, false, 7))) {
                    // line 8
                    echo "                    <img src=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/non-disponible.jpg"), "html", null, true);
                    echo "\" class=\"card-img-top img-fluid\" alt=\"...\">
                ";
                } else {
                    // line 10
                    echo "                    <img src=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["trick"], "imgDocs", [], "any", false, false, false, 10), 0, [], "array", false, false, false, 10))), "html", null, true);
                    echo "\" class=\"card-img-top img-fluid\"
                         alt=\"...\">
                ";
                }
                // line 13
                echo "                <div class=\"card-body ajax-card\">
                    <a class=\"card-title\" href=\"";
                // line 14
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_show", ["slug" => twig_get_attribute($this->env, $this->source, $context["trick"], "slugname", [], "any", false, false, false, 14), "id" => twig_get_attribute($this->env, $this->source, $context["trick"], "id", [], "any", false, false, false, 14)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["trick"], "name", [], "any", false, false, false, 14), "html", null, true);
                echo " </a>
                    <a href=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["trick"], "id", [], "any", false, false, false, 15)]), "html", null, true);
                echo "\">
                        <i class=\"fas fa-pencil-alt\"></i>
                    </a>
                    <form method=\"post\" action=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["trick"], "id", [], "any", false, false, false, 18)]), "html", null, true);
                echo "\"
                          onsubmit=\"return confirm('Are you sure you want to delete this item?');\">
                        <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                        <input type=\"hidden\" name=\"_token\" value=\"";
                // line 21
                echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("delete" . twig_get_attribute($this->env, $this->source, $context["trick"], "id", [], "any", false, false, false, 21))), "html", null, true);
                echo "\">
                        <button id=\"trash-icon\"><i class=\"far fa-trash-alt\"></i></button>
                    </form>
                </div>
            </div>
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trick'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "trick/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 21,  90 => 18,  84 => 15,  78 => 14,  75 => 13,  68 => 10,  62 => 8,  60 => 7,  56 => 5,  51 => 4,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if not tricks|default %}
    {{ count[0].count }}
{% else %}
    {% for trick in tricks %}
        <div class=\"col-12 col-md-6 col-lg-3 m-0 p-3\">
            <div class=\"card\">
                {% if trick.imgDocs is empty %}
                    <img src=\"{{ asset('img/non-disponible.jpg') }}\" class=\"card-img-top img-fluid\" alt=\"...\">
                {% else %}
                    <img src=\"{{ asset('uploads/images/' ~ trick.imgDocs[0]) }}\" class=\"card-img-top img-fluid\"
                         alt=\"...\">
                {% endif %}
                <div class=\"card-body ajax-card\">
                    <a class=\"card-title\" href=\"{{ path('trick_show', {'slug' : trick.slugname, 'id' : trick.id}) }}\">{{ trick.name }} </a>
                    <a href=\"{{ path('trick_edit',{id: trick.id }) }}\">
                        <i class=\"fas fa-pencil-alt\"></i>
                    </a>
                    <form method=\"post\" action=\"{{ path('trick_delete', {'id': trick.id}) }}\"
                          onsubmit=\"return confirm('Are you sure you want to delete this item?');\">
                        <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                        <input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('delete' ~ trick.id) }}\">
                        <button id=\"trash-icon\"><i class=\"far fa-trash-alt\"></i></button>
                    </form>
                </div>
            </div>
        </div>
    {% endfor %}
{% endif %}", "trick/ajax.html.twig", "/usr/share/nginx/html/templates/trick/ajax.html.twig");
    }
}
