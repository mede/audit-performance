<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* trick/show.html.twig */
class __TwigTemplate_accb103d100f9e5a26698cf80f00e61f6954295e3916c25cd21a232c90875b7e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'cover' => [$this, 'block_cover'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "trick/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Trick";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_cover($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <div class=\"card card-image\"
         style=\"background-image: url('";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 9, $this->source); })()), "getFirstImg", [], "any", false, false, false, 9))), "html", null, true);
        echo "'), url('";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/non-disponible.jpg"), "html", null, true);
        echo "');background-repeat: no-repeat; background-size: cover; background-position: center;\">
        <div class=\"text-white text-center rgba-stylish-strong py-5 px-4\">
            <div class=\"edit-delete-btn\">
                <a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_edit", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 12, $this->source); })()), "id", [], "any", false, false, false, 12)]), "html", null, true);
        echo "\" class=\"btn btn-warning btn-sm\">edit</a>
                ";
        // line 13
        echo twig_include($this->env, $context, "member/_delete_form.html.twig");
        echo "
            </div>
            <div class=\"py-5\">
                <h2 class=\"h2 orange-text\"><i class=\"fas fa-snowboarding\"></i></h2>
                <h1 class=\"card-title h2 my-4 py-2\">";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 17, $this->source); })()), "name", [], "any", false, false, false, 17), "html", null, true);
        echo "</h1>
            </div>
        </div>
    </div>
    <div class=\"docs-container row text-center\">
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 22, $this->source); })()), "getImgDocs", [], "method", false, false, false, 22));
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 23
            echo "            <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
                <a>
                    <img src=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . $context["file"])), "html", null, true);
            echo "\" class=\"thumbnails-docs\" data-toggle=\"modal\"
                         data-target=\"#modal";
            // line 26
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" alt=\"\">
                </a>
                <div class=\"modal fade\" id=\"modal";
            // line 28
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\"
                     aria-labelledby=\"";
            // line 29
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" aria-hidden=\"true\">
                    <div class=\"modal-dialog modal-lg\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-body mb-0 p-0\">
                                <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
                                    <img src=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . $context["file"])), "html", null, true);
            echo "\" class=\"embed-responsive-item\"
                                         alt=\"\">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 43, $this->source); })()), "getVideoDocs", [], "method", false, false, false, 43));
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 44
            echo "            <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
                <a>
                    <img src=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/video-icon.jpg"), "html", null, true);
            echo "\" class=\"thumbnails-docs\" data-toggle=\"modal\"
                         data-target=\"#modal";
            // line 47
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" alt=\"\">
                </a>
                <div class=\"modal fade\" id=\"modal";
            // line 49
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\"
                     aria-labelledby=\"";
            // line 50
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" aria-hidden=\"true\">
                    <div class=\"modal-dialog modal-lg\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-body mb-0 p-0\">
                                <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
                                    <video class=\"embed-responsive-item\" controls>
                                        <source src=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . $context["file"])), "html", null, true);
            echo "\" type=\"video/mp4\">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "    </div>
    <div class=\"card card-cascade wider reverse\">
        <div class=\"card-body card-body-cascade text-center\">
            <h4 class=\"card-title\"><strong>Description</strong></h4>
            <p class=\"card-text\">";
        // line 69
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 69, $this->source); })()), "description", [], "any", false, false, false, 69), "html", null, true);
        echo "</p>
            <hr>
            <div class=\"trick-info-collapse\">
                <p>
                    <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\"
                            data-target=\"#multiCollapseExample1\" aria-expanded=\"false\"
                            aria-controls=\"multiCollapseExample2\">Niveau
                    </button>
                    <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\"
                            data-target=\"#multiCollapseExample2\" aria-expanded=\"false\"
                            aria-controls=\"multiCollapseExample2\">Catégorie
                    </button>
                    <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\"
                            data-target=\"#multiCollapseExample3\" aria-expanded=\"false\"
                            aria-controls=\"multiCollapseExample2\">Date d'ajout
                    </button>
                </p>
                <div class=\"row\">
                    <div class=\"col\">
                        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample1\">
                            <div class=\"card card-body alert alert-dark\">
                                ";
        // line 90
        echo twig_escape_filter($this->env, (isset($context["niveau"]) || array_key_exists("niveau", $context) ? $context["niveau"] : (function () { throw new RuntimeError('Variable "niveau" does not exist.', 90, $this->source); })()), "html", null, true);
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"col\">
                        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample2\">
                            <div class=\"card card-body alert alert-dark\">
                                ";
        // line 97
        echo twig_escape_filter($this->env, (isset($context["trick_group"]) || array_key_exists("trick_group", $context) ? $context["trick_group"] : (function () { throw new RuntimeError('Variable "trick_group" does not exist.', 97, $this->source); })()), "html", null, true);
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"col\">
                        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample3\">
                            <div class=\"card card-body alert alert-dark\">
                                ";
        // line 104
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 104, $this->source); })()), "datecreation", [], "any", false, false, false, 104), "d/m/Y"), "html", null, true);
        echo "
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"comments-container\">
        ";
        // line 113
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 114
            echo "            <div>
                ";
            // line 115
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 115, $this->source); })()), 'form_start');
            echo "
                <div class=\"comment-input text-center\">
                    ";
            // line 117
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 117, $this->source); })()), "content", [], "any", false, false, false, 117), 'widget', ["attr" => ["class" => "form-control"]]);
            echo "
                    <button class=\"btn btn-indigo\" id=\"jq-insert\">Envoyer</button>
                </div>
                ";
            // line 120
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 120, $this->source); })()), 'form_end');
            echo "
            </div>
        ";
        }
        // line 123
        echo "        <div id=\"comments_block\" class=\"text-center\"></div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 126
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 127
        echo "    <script type=\"text/javascript\">
      \$(document).ready(function () {
        \$.post(\"";
        // line 129
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_comments", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 129, $this->source); })()), "id", [], "any", false, false, false, 129)]), "html", null, true);
        echo "\")
            .done(function (comments) {
              \$(\"#comments_block\").empty().append(comments);
              \$(\"#jq-plus\").on(\"click\", function () {
                let number_com = \$(\"#jq-com > div\").length;
                \$.post(\"";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_comments", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 134, $this->source); })()), "id", [], "any", false, false, false, 134)]), "html", null, true);
        echo "\", {first: number_com})
                    .done(function (comms) {
                      \$(comms).insertBefore(\"#jq-plus\");
                      if (!\$(comms).hasClass(\"more\")) {
                        \$(\"#jq-plus\").hide();
                      }
                    });
              });
            });
      });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "trick/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 134,  338 => 129,  334 => 127,  324 => 126,  312 => 123,  306 => 120,  300 => 117,  295 => 115,  292 => 114,  290 => 113,  278 => 104,  268 => 97,  258 => 90,  234 => 69,  228 => 65,  213 => 56,  204 => 50,  200 => 49,  195 => 47,  191 => 46,  187 => 44,  182 => 43,  167 => 34,  159 => 29,  155 => 28,  150 => 26,  146 => 25,  142 => 23,  138 => 22,  130 => 17,  123 => 13,  119 => 12,  111 => 9,  108 => 8,  98 => 7,  80 => 5,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Trick{% endblock %}

{% block cover %}{% endblock %}

{% block body %}
    <div class=\"card card-image\"
         style=\"background-image: url('{{ asset('uploads/images/' ~ trick.getFirstImg) }}'), url('{{ asset('img/non-disponible.jpg') }}');background-repeat: no-repeat; background-size: cover; background-position: center;\">
        <div class=\"text-white text-center rgba-stylish-strong py-5 px-4\">
            <div class=\"edit-delete-btn\">
                <a href=\"{{ path('trick_edit', {'id': trick.id}) }}\" class=\"btn btn-warning btn-sm\">edit</a>
                {{ include('member/_delete_form.html.twig') }}
            </div>
            <div class=\"py-5\">
                <h2 class=\"h2 orange-text\"><i class=\"fas fa-snowboarding\"></i></h2>
                <h1 class=\"card-title h2 my-4 py-2\">{{ trick.name }}</h1>
            </div>
        </div>
    </div>
    <div class=\"docs-container row text-center\">
        {% for file in trick.getImgDocs() %}
            <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
                <a>
                    <img src=\"{{ asset('uploads/images/' ~ file) }}\" class=\"thumbnails-docs\" data-toggle=\"modal\"
                         data-target=\"#modal{{ (file|split('.')|first) }}\" alt=\"\">
                </a>
                <div class=\"modal fade\" id=\"modal{{ (file|split('.')|first) }}\" tabindex=\"-1\" role=\"dialog\"
                     aria-labelledby=\"{{ (file|split('.')|first) }}\" aria-hidden=\"true\">
                    <div class=\"modal-dialog modal-lg\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-body mb-0 p-0\">
                                <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
                                    <img src=\"{{ asset('uploads/images/' ~ file) }}\" class=\"embed-responsive-item\"
                                         alt=\"\">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
        {% for file in trick.getVideoDocs() %}
            <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
                <a>
                    <img src=\"{{ asset('img/video-icon.jpg') }}\" class=\"thumbnails-docs\" data-toggle=\"modal\"
                         data-target=\"#modal{{ (file|split('.')|first) }}\" alt=\"\">
                </a>
                <div class=\"modal fade\" id=\"modal{{ (file|split('.')|first) }}\" tabindex=\"-1\" role=\"dialog\"
                     aria-labelledby=\"{{ (file|split('.')|first) }}\" aria-hidden=\"true\">
                    <div class=\"modal-dialog modal-lg\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-body mb-0 p-0\">
                                <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
                                    <video class=\"embed-responsive-item\" controls>
                                        <source src=\"{{ asset('uploads/images/' ~ file) }}\" type=\"video/mp4\">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
    <div class=\"card card-cascade wider reverse\">
        <div class=\"card-body card-body-cascade text-center\">
            <h4 class=\"card-title\"><strong>Description</strong></h4>
            <p class=\"card-text\">{{ trick.description }}</p>
            <hr>
            <div class=\"trick-info-collapse\">
                <p>
                    <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\"
                            data-target=\"#multiCollapseExample1\" aria-expanded=\"false\"
                            aria-controls=\"multiCollapseExample2\">Niveau
                    </button>
                    <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\"
                            data-target=\"#multiCollapseExample2\" aria-expanded=\"false\"
                            aria-controls=\"multiCollapseExample2\">Catégorie
                    </button>
                    <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\"
                            data-target=\"#multiCollapseExample3\" aria-expanded=\"false\"
                            aria-controls=\"multiCollapseExample2\">Date d'ajout
                    </button>
                </p>
                <div class=\"row\">
                    <div class=\"col\">
                        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample1\">
                            <div class=\"card card-body alert alert-dark\">
                                {{ niveau }}
                            </div>
                        </div>
                    </div>
                    <div class=\"col\">
                        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample2\">
                            <div class=\"card card-body alert alert-dark\">
                                {{ trick_group }}
                            </div>
                        </div>
                    </div>
                    <div class=\"col\">
                        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample3\">
                            <div class=\"card card-body alert alert-dark\">
                                {{ trick.datecreation|date('d/m/Y') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"comments-container\">
        {% if is_granted('IS_AUTHENTICATED_FULLY') %}
            <div>
                {{ form_start(form) }}
                <div class=\"comment-input text-center\">
                    {{ form_widget(form.content, { 'attr': {'class': 'form-control'} }) }}
                    <button class=\"btn btn-indigo\" id=\"jq-insert\">Envoyer</button>
                </div>
                {{ form_end(form) }}
            </div>
        {% endif %}
        <div id=\"comments_block\" class=\"text-center\"></div>
    </div>
{% endblock %}
{% block javascripts %}
    <script type=\"text/javascript\">
      \$(document).ready(function () {
        \$.post(\"{{ path('new_comments', {id:trick.id}) }}\")
            .done(function (comments) {
              \$(\"#comments_block\").empty().append(comments);
              \$(\"#jq-plus\").on(\"click\", function () {
                let number_com = \$(\"#jq-com > div\").length;
                \$.post(\"{{ path('new_comments',{id:trick.id}) }}\", {first: number_com})
                    .done(function (comms) {
                      \$(comms).insertBefore(\"#jq-plus\");
                      if (!\$(comms).hasClass(\"more\")) {
                        \$(\"#jq-plus\").hide();
                      }
                    });
              });
            });
      });
    </script>
{% endblock %}
", "trick/show.html.twig", "/usr/share/nginx/html/templates/trick/show.html.twig");
    }
}
