<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/register' => [[['_route' => 'user_registration', '_controller' => 'App\\Controller\\RegistrationController::registerAction'], null, null, null, false, false, null]],
        '/forgotten_pass' => [[['_route' => 'forgotten_password', '_controller' => 'App\\Controller\\RegistrationController::forgottenPassword'], null, null, null, false, false, null]],
        '/reset_pass' => [[['_route' => 'reset_password', '_controller' => 'App\\Controller\\RegistrationController::resetPassword'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'trick_index', '_controller' => 'App\\Controller\\TrickController::index'], null, ['GET' => 0], null, false, false, null]],
        '/member/new' => [[['_route' => 'trick_new', '_controller' => 'App\\Controller\\TrickController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/ajax' => [[['_route' => 'trick_ajax', '_controller' => 'App\\Controller\\TrickController::ajax'], null, ['POST' => 0], null, true, false, null]],
        '/member/profile-details' => [[['_route' => 'profil_show', '_controller' => 'App\\Controller\\UserController::show'], null, null, null, false, false, null]],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'logout'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/([a-z0-9\\-]*)\\-([^/]++)(*:31)'
                .'|/member/(?'
                    .'|([^/]++)/(?'
                        .'|edit(*:65)'
                        .'|delete(*:78)'
                    .')'
                    .'|profile/([^/]++)(*:102)'
                .')'
                .'|/new_comments/([^/]++)(*:133)'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:172)'
                    .'|wdt/([^/]++)(*:192)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:238)'
                            .'|router(*:252)'
                            .'|exception(?'
                                .'|(*:272)'
                                .'|\\.css(*:285)'
                            .')'
                        .')'
                        .'|(*:295)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        31 => [[['_route' => 'trick_show', '_controller' => 'App\\Controller\\TrickController::show'], ['slug', 'id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        65 => [[['_route' => 'trick_edit', '_controller' => 'App\\Controller\\TrickController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        78 => [[['_route' => 'trick_delete', '_controller' => 'App\\Controller\\TrickController::delete'], ['id'], ['DELETE' => 0], null, false, false, null]],
        102 => [[['_route' => 'profil_edit', '_controller' => 'App\\Controller\\UserController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        133 => [[['_route' => 'new_comments', '_controller' => 'App\\Controller\\TrickController::newComments'], ['id'], ['POST' => 0], null, false, true, null]],
        172 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        192 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        238 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        252 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        272 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        285 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        295 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
